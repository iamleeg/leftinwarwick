---
layout: post
title:  "The Ethics of Climate Change: Right and Wrong in a Warming World"
date:   2020-03-29 14:00:00 +0100
categories: event leamington
---
Despite the Coronavirus lockdown, we still "met" (virtually, using [Jit.si](https://jitsi.org)) to discuss Doughnut Economics. We wanted to pick a book that we could all definitely get access to next time, despite the library being closed, bookshops being closed, and none of us wanting to cosy up to Jeff Bezos.

The Internet Archive has made a fantastic resource available; for the duration of the U.S. national Coronavirus emergency, the [National Emergency Library](https://blog.archive.org/2020/03/24/announcing-a-national-emergency-library-to-provide-digitized-books-to-students-and-the-public/) provides books from the U.S. education curriculum free, worldwide.

Our next book is chosen from its catalogue: [The Ethics of Climate Change](https://archive.org/details/ethicsofclimatec0000garv) by James Garvey. We're meeting at 12:00 BST on Sunday 26 April: online if needed (get in touch for the link) or at Leamington Spa Library if possible.
