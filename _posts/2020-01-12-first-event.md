---
layout: post
title:  "First Event!"
date:   2020-01-12 15:20:00 +0000
categories: event leamington
---
Our first Left in Warwick reading group will be in the [Muse coffee house](http://www.themusecoffeehouse.co.uk/), Regent Grove, Leamington Spa. We'll meet at 11:00 on Sunday, 26th January.

There's no set text for this first meeting. Please just come along with your interest, your recommendations, and your enthusiasm to read and debate left books! Any other questions? [Send me an email.](mailto:graham@iamleeg.com)

[Add this event to your calendar.](/assets/cal/liw-1.ics)
