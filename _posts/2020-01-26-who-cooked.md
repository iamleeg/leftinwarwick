---
layout: post
title:  "Who Cooked Adam Smith’s Dinner?"
date:   2020-01-26 18:20:00 +0000
categories: event leamington
---
Our first meeting today went well! We decided that there are a lot of topics we’d like to explore, including:

 - political philosophy
 - feminist and anti-racist socialism
 - socialism and the rights of the individual

Our next meeting will be in [Dinky’s Play Café](https://www.facebook.com/dinkysplaycafe/), 66 Bath Street, Leamington Spa, on [Saturday 22nd Feb at 11:00](/assets/cal/liw-2.ics). We’re discussing [Who Cooked Adam Smith’s Dinner?: A Story About Women and Economics](https://www.goodreads.com/book/show/23206098-who-cooked-adam-smith-s-dinner) by Katrine Marçal. You're welcome to join us!
