---
layout: post
title:  "Welcome to Left in Warwick!"
date:   2020-01-05 18:22:27 +0000
categories: meta update
---
I'm talking with [Left Book Club](https://leftbookclub.com) about setting up a chapter here. When that's up and running, left-minded Warwick-and-Leamingtonians (and anyone else who wants to drop in) will have a place to read, debate, and organise. If this sounds interesting to you, [let me know](mailto:graham@iamleeg.com) particularly what days/times work for you, favoured venues, that sort of thing.

More details as they become defined!
