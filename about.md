---
layout: page
title: About
permalink: /about/
---

Left in Warwick hosts Warwick and Leamington's [Left Book Club](https://leftbookclub.com) reading group. If you're in the area, check the date of our next meetup and come along to talk socialism and books! You don't need to be an expert on socialist theory and history - I'm certainly not - just interested in reading and discussing how we'll make the world better for the many.

Text content on this site is freely redistributable under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/"). Please check captions for individual images for their redistribution terms.

![Creative Commons Licence](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
